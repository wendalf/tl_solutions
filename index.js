// Import node modules
const fs = require('fs');
const csv = require('csv-parser');
const axios = require('axios');

// Variables declaration
let impPixelSuccess = 0,
	impPixelFailed 	= 0,
	failedTactics 	= {},
	summary 		= '';

// Parse CSV and clean data set
fs.createReadStream(`${__dirname}/tactic.csv`)
	.pipe(csv())
	.on('data', (data) => {
		let tacticId 			= data.tactic_id,
			impressionPixelJson = data.impression_pixel_json;
		if (impressionPixelJson !== 'NULL' && impressionPixelJson !== '[]') {
			let impPixels = JSON.parse(impressionPixelJson);
			impPixels.forEach((impPixel) => {
				getHttpResponseOf(tacticId, impPixel);
			});
		}
	})
	.on('end', () => {
		console.log("CSV Reading Done!");
	});

// Create Axios instance and set timeout
let axiosInstance = axios.create();
axiosInstance.defaults.timeout = 5000;

// Get HTTP response of the pixel and count the result
function getHttpResponseOf(tacticId, impPixel) {
	axiosInstance.get(impPixel)
	.then((response) => {
		if (response.status < 400) {
			impPixelSuccess += 1;
			console.log('Response Status:', response.status, 'Success NO.', impPixelSuccess);
			return;
		}
	})
	.catch((error) => {
		impPixelFailed += 1;
		failedTactics[tacticId] = failedTactics[tacticId] || [];
		failedTactics[tacticId].push(impPixel);
		console.log('Error NO.', impPixelFailed);
		return;
	});
}

// Log out the summary
process.on('exit', () => {
	console.log('Tactic ID & URLs that failed: ', failedTactics);
	console.log('Number OK: ', impPixelSuccess);
	console.log('Number Failed: ', impPixelFailed);
	summary = `Number OK: ${impPixelSuccess}\n Number Failed: ${impPixelFailed}\n Tactic ID & URLs that failed:\n ${failedTactics}`;
	return summary;
});

module.exports = summary;