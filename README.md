# TL Solutions Challenge

Write a script that will programmatically check whether all impressions pixels are valid.

## Summary
TL Solutions want to make sure that all Tactics' impression tracking pixels are valid. The NodeJS script in this repo will help TL Solutions to identify faulty trackers.

The script will log the HTTP response code or "Error" in the terminal while running. It will output a summary of the results including the Number OK (2xx and 3xx responses), the Number Failed (4xx and 5xx response), and the list of Tactic ID and URLs that failed when it finishes.

## Install

### Install Node
If you have node installed, you can skip the following set up instructions.

* If you have homebrew run the following code in your terminal.
```
brew install node 
```
* Next we'll install the node version manager 'n'. You can install this with:
```
npm install -g n
```
* Get the latest version of node with:
```
n latest
```

### Clone or download this repo
After you clone or download this repo, navigate into the repo's folder in terminal.

### Install dependencies
Make sure to update/install package dependencies before you run the script. Run the following code in your terminal. (Make sure you are in the repo's folder).
```
npm install
```

#### Package dependencies

* axios
* csv-parser
* mocha
* chai

## Run NodeJS Script

Run the following code in your terminal.

```
npm run script
```
Watch your terminal for logging messages and the final summary.


## Run Test
```
npm test
```
