Analyze this data to diagnose the source of the data misalignment and issue a correspondence to communicate this out to relevant parties (so who are the internal or external parties? what do you tell them?).

________________________________________________

### Internal Party - Thomas from Client Services

Hi Thomas,

Thank you for bringing the data discrepancy related with tactic 343664 to our attention.

I looked at the log level dataset you sent and found out that the one specific domain, politicalwire.com, is missing data entries on the Client Log DataSet. I believe the third-party tracking pixel has caused this discrepancy. My theory is either the pixel was not injected correctly, or the pixel did not fire accurately. This would explain why TL's number is higher than the client's number.

I will reach out to the publisher's AM to confirm that our JS tag is on the page. I will also verify with the third-party tracking pixel provider to make sure that their pixel is working correctly. I'll let you know when I find out more details.

Thanks again,

Wendy

________________________________________________

### Internal Party - politicalwire.com's AM

Hi,

Hope you are doing well! Thomas found a data discrepancy between TL's data and one of our buyers' data that I'd like to bring to your attention.

The discrepancy exists on tactic 343664's impressions for politicalwire.com. Could you please verify with your contact there that our JS tag is appropriately placed on their page?

Thank you very much for your help, and please let me know if you have any questions.

Best,

Wendy

________________________________________________

### External Party - Third-party tracking pixel provider

Hi,

Happy Friday! I'm reaching out to address the data discrepancy issue between our log level dataset and yours for the Land Rover Campaign. The impressions delivered on politicalwire.com are recorded on our analytics collection engine but are absent from entries on your log level dataset.

Could you please help us to check if the tracking pixel, http://ec2-50-16-41-235.compute-1.amazonaws.com:8002/support.php, is firing correctly? Please let me know what you find when you get a chance.

Thank you for your help, and please let me know if you have any questions.

Best,
Wendy