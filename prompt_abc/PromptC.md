Run simulations using this forced mechanism and exploring pixel drops to confirm your suspicions from part 1. Provide a clear recommendation to the relevant stakeholders/parties.

________________________________________________

### Internal Party - Thomas from Client Services

Hi Thomas,

I've confirmed my theory by running simulations using our forced mechanism and exploring pixel drops on politicalwire.com.

I found a canceled request by inspecting the network activity (please see screenshot: canceled_request.png).  I also found a 'mixed content' warning when TL's tag was loaded over HTTPS but requested the pixel over HTTP (please see screenshot: mixed_content.png). This tracking pixel script is blockable by the browser, which would cause the pixel not firing correctly.  

I will address this issue with the tracking pixel provider and ask them to provide Land Rover an SSL compatible tracking pixel.

Thank you for your patience. I'll keep you posted about any progress I make.

Best,

Wendy

________________________________________________

### External Party - Third-Party tracking pixel provider

Hi,

Hope you are doing well!

I'm reaching out to let you know that I've confirmed my suspicion on why there's a discrepancy between our data and yours for the Land Rover campaign.

The tracking pixel didn't fire accurately when it's being dropped on https://politicalwire.com. I found a 'mixed content' warning when the page was loaded over HTTPS but requested the pixel over HTTP (please see screenshot: mixed_content.png).

Could you please look into this and provide Land Rover an SSL compatible tracking pixel? 

Thank you so much for your help, and please don't hesitate to reach out for any questions.

Best,

Wendy