const chai = require('chai');
const expect = chai.expect;
const summary = require('../index');

describe('Script', function(){
	it('Summary should return a string', function() {
		expect(summary).to.be.a('string');
	});
});